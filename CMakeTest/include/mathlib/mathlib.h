
#ifdef __cplusplus
extern "C"{
#endif

void calculate(double,double,const char *);
const char * operationsstring();
const char * examplesstring();

#ifdef __cplusplus
}
#endif
