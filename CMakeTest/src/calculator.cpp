#include <iostream>
#include <string>
#include <stdexcept>
#include <mathlib.h>
using namespace std;

void printusage(){
	cout << endl
		 <<	"Usage: Calculator NUMBER OPERATION (NUMBER)" << endl
		 <<	"OPERATION can be : " << operationsstring() << endl
		 <<	"Examples:" << examplesstring() << endl
		 << endl;
}
int main(int argc, const char *argv[])
{
	if ((argc != 4) && (argc != 3)) {
		printusage();
		return 1;
	}
	double a=-1,b=-1;
	try {
		a=stod(argv[1]);
	}  catch (const invalid_argument& ia) {
		cerr << "ERROR: Invalid first argument: " << ia.what() << '\n';
		printusage();
	}
	if(argc==4)
		try {
			b=stod(argv[3]);
		}  catch (const invalid_argument& ia) {
			cerr << "WARNING: Invalid second argument: " << ia.what() << '\n';
			b=-1;
		}
	calculate(a,b,argv[2]);
	return 0;
}
