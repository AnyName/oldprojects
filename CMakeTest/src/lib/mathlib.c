#include <math.h>	//math functions

#include <stdio.h>	//printf
#include <string.h>	//strlen, strcmp
#include <ctype.h>	//tolower
#include <stdlib.h>	//malloc, free
#include <mathlib.h>
enum operationsenum {
	//2 arguments
	ADD,
	SUBTRACT,
	MULTIPLY,
	DIVIDE,
	POWER,
	//1 argument
	SQRT,
	LOGE,
	LOG10,
	EXP,
	SIN,
	COS,
	TAN,
	ASIN,
	ACOS,
	ATAN
};
char * OperationsCharArray[] = {

	"+",
	"-",
	"*",
	"/",
	"^",

	"sqrt",
	"ln",
	"log10",
	"e",
	"sin",
	"cos",
	"tan",
	"asin",
	"acos",
	"atan",
	0
};
const char * examplesstring(){
	return	"\n\t'Calculator 5 + 5' Will return ' = 10.000000'"
			"\n\t'Calculator 9 sqrt' Will return ' = 3.000000'";
}
const char * operationsstring() {
	return	"\nSupports 2 arguments (second will be defaulted to -1 if undefined)"
			"\n\tAdd: '+'"
			"\n\tSubtract: '-'"
			"\n\tMultiply: '*'"
			"\n\tDivide: '/'"
			"\n\tPower: '^'"
			"\nSupports only 1 argument (second will be ignored)"
			"\n\tSquare Root: 'sqrt'"
			"\n\tNatural (E) Logarithm: 'ln'"
			"\n\tCommon (10) Logarithm: 'log10'"
			"\n\tExponent power: 'e'"
			"\n\tSine: 'sin'"
			"\n\tCosine: 'cos'"
			"\n\tTangent: 'tan'"
			"\n\tArc Sine: 'asin'"
			"\n\tArc Cosine: 'acos'"
			"\n\tArc Tangent: 'atan'"
			"\n"
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
		"All operations can be used without quotes.\0";
#else
		"Almost every operation (with exception being multiply because shell will replace asterisk with contents of the folder) can be used without quotes.\0";
#endif
}

char * lower(const char * str){
	char * out =malloc(strlen(str)+1);
	int i=0;
	while(str[i]){
		out[i]=tolower(str[i]);
		i++;
	}
	return out;
}

int operatortooperation(const char* operator){
	int i=0;
	char * lowoperator=lower(operator);
	while(OperationsCharArray[i]) {
		if(strcmp(OperationsCharArray[i], lowoperator) == 0) {
			free(lowoperator);
			return i;
		}
		i++;
	}
	free(lowoperator);
	return -1;
}

void calculate(double a,double b,const char * operation){
	double out;
	int openum=operatortooperation(operation);
	if((openum>POWER) && (b!=-1)) printf("Ignoring second argument '%lf' because operation '%s' supports only 1 argument\n",b,operation);
	switch (openum) {
		case ADD:
			out=a+b;
		break;
		case SUBTRACT:
			out=a-b;
		break;
		case MULTIPLY:
			out=a*b;
		break;
		case DIVIDE:
			if(b==0){
				printf("ERROR: Division by 0\n");
				return;
			}
			out=a/b;
		break;
		case POWER:
			out=pow(a,b);
		break;
		case SQRT:
			out=sqrt(a);
		break;
		case LOGE:
			out=log(a);
		break;
		case LOG10:
			out=log10(a);
		break;
		case EXP:
			out=exp(a);
		break;
		case SIN:
			out=sin(a);
		break;
		case COS:
			out=cos(a);
		break;
		case TAN:
			out=tan(a);
		break;
		case ASIN:
			out=asin(a);
		break;
		case ACOS:
			out=acos(a);
		break;
		case ATAN:
			out=atan(a);
		break;
		default:
			printf("ERROR: Invalid operator or operation.\n");
			return;
		break;
	}
	printf(" = %f\n",out);
	return;
}
