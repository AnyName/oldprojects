#-------------------------------------------------
#
# Project created by QtCreator 2016-11-02T13:05:03
#
#-------------------------------------------------

QT       += core gui network widgets

TARGET = ClientTest
TEMPLATE = app


SOURCES += main.cpp\
        client.cpp

HEADERS  += client.hpp

FORMS    += client.ui
