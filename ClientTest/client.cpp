#include "client.hpp"
#include "ui_client.h"
#include <QTcpSocket>
#include <QHostAddress>
Client::Client(QWidget *parent) :
QMainWindow(parent),
ui(new Ui::Client)
{
	ui->setupUi(this);
}

Client::~Client()
{
	delete ui;
}

void Client::changeEvent(QEvent *e)
{
	QMainWindow::changeEvent(e);
	switch (e->type()) {
	case QEvent::LanguageChange:
		ui->retranslateUi(this);
		break;
	default:
		break;
	}
}

void Client::on_ConnectButton_clicked()
{
	QTcpSocket *socket=new QTcpSocket;
	QHostAddress host(ui->Address->text());
	socket->connectToHost(host, ui->Port->text().toInt());
	if(socket->waitForConnected(3000)){
		ui->Console->append((QString)"CLIENT: Message \"" + ui->Message->text() +(QString)"\" was sent");
		socket->write(ui->Message->text().toStdString().c_str());
		socket->waitForBytesWritten(500);
		socket->waitForReadyRead(1500);
		ui->Console->append((QString)"CLIENT: Got message:  \""+QString(socket->readAll()) + '\"');
		socket->close();
	}
}
