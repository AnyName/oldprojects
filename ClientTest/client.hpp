#ifndef CLIENT_HPP
#define CLIENT_HPP

#include <QtWidgets/QMainWindow>
//#include<QString>
namespace Ui {
class Client;
}

class Client : public QMainWindow
{
	Q_OBJECT

public:
	explicit Client(QWidget *parent = 0);
	~Client();

protected:
	void changeEvent(QEvent *e);
	QString port,address;

private slots:
	void on_ConnectButton_clicked();

private:
	Ui::Client *ui;
};

#endif // CLIENT_HPP
