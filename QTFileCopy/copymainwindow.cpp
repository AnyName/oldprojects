#include "copymainwindow.hpp"
#include "ui_copymainwindow.h"
CopyMainWindow::CopyMainWindow(QWidget *parent) :
QMainWindow(parent),
ui(new Ui::CopyMainWindow)
{
	ui->setupUi(this);
	QStringList args=QCoreApplication::arguments();
	fch=new FileCopyHelper(args[1],args[2]);
	ui->progressBar->setRange(0,100);
	ui->progressBar->setValue(0);
	ui->cancelButton->setHidden(1);
	ui->pauseButton->setGeometry(130,40,100,20);
	paused=false;
	started=false;
	connect(fch,SIGNAL(progressChanged() ),this,SLOT(fileProgress() ) );
	connect(this,SIGNAL(intValue(int) ),ui->progressBar,SLOT(setValue(int) ) );
	connect(fch,SIGNAL(done() ),this,SLOT(close() ) );
}

CopyMainWindow::~CopyMainWindow()
{
	delete ui;
}

void CopyMainWindow::changeEvent(QEvent *e)
{
	QMainWindow::changeEvent(e);
	switch (e->type() ) {
	case QEvent::LanguageChange:
		ui->retranslateUi(this);
		break;
	default:
		break;
	}
}

void CopyMainWindow::fileProgress(){
	emit intValue( (int) ( (qreal)fch->progress() * (qreal) 100 ) );
}

void CopyMainWindow::on_pauseButton_clicked(){
	if(started){
		if(paused) ui->pauseButton->setText("Pause");
		else ui->pauseButton->setText("Resume");
		fch->pause();
		paused=!paused;
	}
	else{
		ui->cancelButton->setHidden(0);
		ui->pauseButton->setGeometry(40,40,80,20);
		ui->pauseButton->setText("Pause");
		started=true;
		fch->begin();
	}
}

void CopyMainWindow::on_cancelButton_clicked()
{
	fch->cancel();
}
