#ifndef COPYMAINWINDOW_HPP
#define COPYMAINWINDOW_HPP

#include <QtWidgets/QMainWindow>
#include "filecopyhelper.hpp"

namespace Ui {
class CopyMainWindow;
}

class CopyMainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit CopyMainWindow(QWidget *parent = 0);
	~CopyMainWindow();
signals:
	void intValue(int);
protected:
	void changeEvent(QEvent *e);
	FileCopyHelper * fch;

private slots:
	void on_pauseButton_clicked();
	void on_cancelButton_clicked();
	void fileProgress();
private:
	bool paused,started;
	Ui::CopyMainWindow *ui;
};

#endif // COPYMAINWINDOW_HPP
