#include "filecopyhelper.hpp"
#include <QDebug>
FileCopyHelper::FileCopyHelper(QString sPath, QString dPath, quint64 bufferSize):isCancelled(false), bufferSize(bufferSize), prog(0.0), source(sPath), destination(dPath), position(0){
	paused=false;
}
FileCopyHelper::~FileCopyHelper() {
	free(buff);
}
qreal FileCopyHelper::progress() const {
	return prog;
}
void FileCopyHelper::setProgress(qreal p) {
	if (p != prog) {
		prog = p;
		emit progressChanged();
	}
}
void FileCopyHelper::pause(){
	if(paused)
		QMetaObject::invokeMethod(this, "step", Qt::QueuedConnection);
	paused=!paused;
}
void FileCopyHelper::begin(){
	if (!source.open(QIODevice::ReadOnly) ) {
		errormessage.critical(0,"ERROR","could not open source, aborting");
		emit done();
		return;
	}
	fileSize = source.size();
	if (!destination.open(QIODevice::WriteOnly) ) {
		errormessage.critical(0,"ERROR","could not open destination, aborting");
		// maybe check for overwriting and ask to proceed
		emit done();
		return;
	}
	if (!destination.resize(fileSize) ) {
		errormessage.critical(0,"ERROR","could not resize, aborting");
		emit done();
		return;
	}
	buff = (char*)malloc(bufferSize);
	if (!buff) {
		errormessage.critical(0,"ERROR","could not allocate buffer, aborting");
		emit done();
		return;
	}
	QMetaObject::invokeMethod(this, "step", Qt::QueuedConnection);
	//timer.start();
}
void FileCopyHelper::step(){
	if (!isCancelled) {
		if (position < fileSize) {
			if(!paused){
				quint64 chunk = fileSize - position;
				quint64 l = chunk > bufferSize ? bufferSize : chunk;
				source.read(buff, l);
				destination.write(buff, l);
				position += l;
				source.seek(position);
				destination.seek(position);
				setProgress((qreal)position / fileSize);
				//std::this_thread::sleep_for(std::chrono::milliseconds(100) ); // for testing
				QMetaObject::invokeMethod(this, "step", Qt::QueuedConnection);
			}
			else return;
		} else {
			//qDebug() << timer.elapsed();
			emit done();
			return;
		}
	} else {
		if (!destination.remove() ) errormessage.warning(0,"Warning!","Deletion failed");
		emit done();
	}
}
void FileCopyHelper::cancel() {
	isCancelled = true;
}
