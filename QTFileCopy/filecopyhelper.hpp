#ifndef FILECOPYHELPER_HPP
#define FILECOPYHELPER_HPP

#include <QtCore/QObject>
#include <QtCore/qglobal.h>
#include <QFile>
#include <QMessageBox>
class FileCopyHelper: public QObject {
public:
	Q_OBJECT
	Q_PROPERTY(qreal progress READ progress WRITE setProgress NOTIFY progressChanged)
public:
	FileCopyHelper(QString, QString, quint64 bufferSize = 1024 * 1024);
	~FileCopyHelper();
	qreal progress() const;
	void setProgress(qreal);
	void pause();
public slots:
	void begin();
	void step();
	void cancel();
signals:
	void progressChanged();
	void done();
private:
	bool paused;
	QMessageBox errormessage;
	bool isCancelled;
	quint64 bufferSize;
	qreal prog;
	QFile source, destination;
	quint64 fileSize, position;
	char * buff;
	//QElapsedTimer timer;
};
#endif // FILECOPYHELPER_HPP
