#include "copymainwindow.hpp"
#include <QApplication>
int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	QMessageBox errormessage;
	if(argc<3){
		errormessage.critical(0,"ERROR","Not enough arguments");
		exit(1);
	}
	else if(argc>3) {
		QStringList skiplist=QApplication::arguments();
		skiplist.removeFirst();
		skiplist.removeFirst();
		skiplist.removeFirst();
		errormessage.warning(0,"Warning","Skipping arguments "+skiplist.join(", ") );
	}
	CopyMainWindow w;
	w.show();

	return a.exec();
}
