#ifndef DBCONTROLLER_HPP
#define DBCONTROLLER_HPP

#include <QObject>
#include <QPoint>
class DBController : public QObject
{
	Q_OBJECT

public:
	explicit DBController();

public slots:
	void editSlot();
	void exportSlot();
	void deleteSlot();
	void fieldSelected(QPoint);

private:
	QPoint field;
};

#endif // DBCONTROLLER_HPP
