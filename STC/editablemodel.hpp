#ifndef EDITABLEMODEL_HPP
#define EDITABLEMODEL_HPP
#include <QAbstractItemModel>
#include <QObject>

class EditableModel : public QAbstractTableModel
{
public:
	/*EditableModel(QObject *parent);
	int rowCount(const QModelIndex &parent = QModelIndex()) const override ;
	int columnCount(const QModelIndex &parent = QModelIndex()) const override;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
	bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole) override;*/
	Qt::ItemFlags flags(const QModelIndex & index) const override ;
private:
	//QVector<QVector<QString>> m_gridData;  //holds text entered into QTableView
signals:
	void editCompleted(const QString &);
};

#endif // EDITABLEMODEL_HPP
