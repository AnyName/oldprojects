#include "mainwindow.hpp"
#include "defines.hpp"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QDir>
#include <QDirIterator>
#include <QDebug>
#include <QtSql>
QStringList MainWindow::supportedTokens = QStringList() << "root" << "texteditor" << "fileformats" << "encoding" << "hasintellisense" << "hasplugins" << "cancompile";
MainWindow::MainWindow(QWidget *parent) :
QMainWindow(parent),
ui(new Ui::MainWindow)
{
	ui->setupUi(this);
	createActions();
	createMenus();
	controller = new DBController();
	//4. Используя контекстное меню, записи в таблице должны удаляться, редактироваться, экспортироваться в XML-файл;
	ui->table->horizontalHeader()-> setContextMenuPolicy(Qt::CustomContextMenu);
	connect(ui->table->horizontalHeader(), SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(CustomContextMenu(QPoint)));
	connect(ui->table->horizontalHeader(), SIGNAL(customContextMenuRequested(QPoint)), controller, SLOT(fieldSelected(QPoint)));

	//5. Вся работа с БД должна производиться отдельном потоке.
	controller->moveToThread(&DBThread);

}

void MainWindow::CustomContextMenu(QPoint & pos){
	QMenu * contextMenu = new QMenu(this);
	//contextMenu->addAction()
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::changeEvent(QEvent *e)
{
	QMainWindow::changeEvent(e);
	switch (e->type()) {
	case QEvent::LanguageChange:
		ui->retranslateUi(this);
		break;
	default:
		break;
	}
}


void MainWindow::openFilesDialog()
{
	//1. Пользователь должен иметь возможность выбрать файлы с XML-файлами с последующим импортом её содержимого
	QStringList FilePath = QFileDialog::getOpenFileNames(this, tr("Open Files"), QString(), "XML Files (*.xml)",nullptr, QFileDialog::DontUseNativeDialog | QFileDialog::ReadOnly | QFileDialog::HideNameFilterDetails );
	foreach (auto i,FilePath) {
		QFuture<void> future = this->openXMLFile( i);
	}
}


void MainWindow::openDirDialog()
{
	//1. Пользователь должен иметь возможность выбрать !папку! с XML-файлами с последующим импортом её содержимого
	QString DirectoryString = QFileDialog::getExistingDirectory(this, tr("Open Directory"), QString(), QFileDialog::DontUseNativeDialog);
	QDirIterator it( DirectoryString, QStringList("*.xml"), QDir::Files | QDir::Readable, QDirIterator::Subdirectories );
	while (it.hasNext()) {
		QFuture<void> future = this->openXMLFile( it.next() );
	}
}

//3. Импорт файлов должен происходить в отдельном потоке;
QFuture<void> MainWindow::openXMLFile(QString file){
	auto worker = [](const QString &file) {
		QFile qfile(file);
		bool open = qfile.open(QIODevice::ReadOnly | QIODevice::Text);
		if (!open)
		{
			qDebug() << "Couldn't open " << file;
		}
		else
		{
			qDebug() << file  << " opened ";
		}
		//Описание было недостаточно понятным. Решил сделать парсер чтобы парсил хмл файлы данные в примере.
		QXmlStreamReader xmlfile ( &qfile );
		QStack<QString> stack;
		while ( !(xmlfile.atEnd()) && !(xmlfile.hasError()) ) {
			QXmlStreamReader::TokenType token = xmlfile.readNext();
			if(token == QXmlStreamReader::StartDocument) {
					continue;
			}
			if(token==QXmlStreamReader::StartElement ) {
				QString tokenstring=xmlfile.name().toString();
				if(supportedTokens.contains(tokenstring) ){
					qDebug() << file <<  " StartElement. Name: " << tokenstring << " level " << stack.size();
					stack.push(tokenstring);
				}
				else {xmlfile.raiseError(" Error: Unsupported Token ");break;}
			}
			if(token==QXmlStreamReader::EndElement ) {
				QString tokenstring=xmlfile.name().toString();
				if(stack.endsWith(tokenstring)){
					stack.pop();
					qDebug() << file <<  " EndElement. Name: " << tokenstring << " level " << stack.size();
				}
				else {xmlfile.raiseError(" Error: EndElement isnt the same as StartElement ");break;}
			}
			//Удаление переноса строки и следующими за ним вайтспэйсов.
			if(xmlfile.tokenType()==QXmlStreamReader::Characters && ( xmlfile.text().toString().simplified().size() == 0)){
				continue;
			}
			if(xmlfile.tokenType()==QXmlStreamReader::Characters)
			{
				qDebug() << file << " Data " << xmlfile.text() << " type " << xmlfile.tokenString()<< " level " << stack.size();
			}
		}
		//2. При парсинге XML-файлов предусмотреть обработку ошибок и уведомление пользователя о них;
		if (xmlfile.hasError()) {
			qDebug() << tr( "Error: %1 in file %2 at line %3, column %4.\n").arg(
							   xmlfile.errorString(), file,
							   QString::number(xmlfile.lineNumber()),
							   QString::number(xmlfile.columnNumber()));
		}
	};
	return QtConcurrent::run(worker, file);
}

void MainWindow::createActions()
{
	openFileAct = new QAction(tr("Open &File"), this);
	openFileAct->setShortcuts(QKeySequence::Find);
	connect(openFileAct, SIGNAL(triggered()), this, SLOT(openFilesDialog()));

	openDirAct = new QAction(tr("Open &Directory"), this);
	openDirAct->setShortcuts(QKeySequence::Open);
	connect(openDirAct, SIGNAL(triggered()), this, SLOT(openDirDialog()));

	exitAct = new QAction(tr("E&xit"), this);
	exitAct->setShortcuts(QKeySequence::Quit);
	connect(exitAct, SIGNAL(triggered()), this, SLOT(close()));


	exportAct = new QAction(tr("Export"), this);
	connect(exportAct, SIGNAL(triggered()), controller, SLOT(exportSlot()));

	editAct = new QAction(tr("Edit"), this);
	connect(editAct, SIGNAL(triggered()), controller, SLOT(editSlot()));

	deleteAct = new QAction(tr("Delete"), this);
	connect(deleteAct, SIGNAL(triggered()), controller, SLOT(deleteSlot()));

}

void MainWindow::createMenus()
{
	Menu = menuBar()->addMenu(tr("&Menu"));
	Menu->addAction(openFileAct);
	Menu->addAction(openDirAct);
	Menu->addAction(exitAct);
}

