#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QtWidgets/QMainWindow>
#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QtConcurrent/QtConcurrent>
#include "dbcontroller.hpp"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	void createActions();
	void createMenus();
	~MainWindow();

public slots:
	void openFilesDialog();
	void openDirDialog();

protected:
	void changeEvent(QEvent *e);

private slots:
	void CustomContextMenu(QPoint &pos);

private:
	Ui::MainWindow *ui;
	QMenu *Menu;
	QAction *openFileAct,*openDirAct,*exitAct,*exportAct,*editAct,*deleteAct;
	QFuture<void> openXMLFile(QString input);
	static QStringList supportedTokens;
	QThread DBThread;
	DBController * controller;
};

#endif // MAINWINDOW_HPP
