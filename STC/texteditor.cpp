#include "texteditor.hpp"

TextEditor::TextEditor()
{

}
TextEditor::~TextEditor(){
	encoding.empty();
}
QStringList TextEditor::supportedTokens = QStringList() << "root" << "texteditor" << "fileformats" << "encoding" << "hasintellisense" << "hasplugins" << "cancompile";
bool TextEditor::setData(QString token,QString data){
	bool * ptr,isbool=false;
	switch (supportedTokens.indexOf(token)) {
		case 1:
			name=data;
		break;
		case 2:
			formats=data;
		break;
		case 3:
			encoding=data.split(';',QString::SkipEmptyParts);
			for(auto& str : encoding)
				str = str.trimmed();
		break;
		case 4:
			ptr=&plugins;
			isbool=true;
		break;
		case 5:
			ptr=&isense;
			isbool=true;
		break;
		case 6:
			ptr=&compiler;
			isbool=true;
		break;
		default:
		break;
	}
	if(isbool){
		QString falsestr="false";
		QString truestr="true";
		*ptr=QString::compare(data, truestr, Qt::CaseInsensitive);
		if( !(*ptr) && !QString::compare(data, falsestr, Qt::CaseInsensitive) ) return false;
	}
	return true;
}
