#ifndef TEXTEDITOR_HPP
#define TEXTEDITOR_HPP

#include <QString>
#include <QFile>
class TextEditor
{
public:
	TextEditor();
	~TextEditor();
	bool setData(QString token,QString data);
	bool save(QFile file);
	QString name,formats;
	QStringList encoding;
	bool isense,plugins,compiler;
	static QStringList supportedTokens;
};

#endif // TEXTEDITOR_HPP
