QT+=core network
CONFIG += c++11

TARGET = Client
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    core.cpp

DISTFILES += \
    settings.ini

HEADERS += \
    core.hpp
