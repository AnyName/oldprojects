#include "core.hpp"
#include <fstream>						//std::ifstream
#include <QDebug>
#include <QTcpSocket>
#include <QHostAddress>
#include <QTime>
#include <QDir>
#include <QRegularExpression>

Core::Core(){
	QFile * file=new QFile("settings.ini");
	ParseOptions(file);
	file->close();
	fsWatcher = new QFileSystemWatcher(this);
	fsWatcher->addPath(options["directory"]);
	QFileInfo f(options["directory"]);
	if(f.isDir() )
	{
		const QDir dirw(options["directory"]);
		contents[options["directory"]] = dirw.entryList(QDir::NoDotAndDotDot | QDir::AllDirs | QDir::Files, QDir::DirsFirst);
		QStringList watcherlist=contents[options["directory"]];
		watcherlist.replaceInStrings(QRegularExpression("^"),options["directory"]+"/");
		fsWatcher->addPaths(watcherlist);
	}
	udpSocket = new QUdpSocket(this);
	udpSocket->bind(4096, QUdpSocket::ShareAddress);
	connect(udpSocket, SIGNAL(readyRead()),this, SLOT(onmessage()));
	connect(fsWatcher, SIGNAL(fileChanged(QString) ), this, SLOT(filechanged(QString) ) );
	connect(fsWatcher, SIGNAL(directoryChanged(QString) ), this, SLOT(directorychanged(QString) ) );
}
QString Core::sendmessage(){
	QString tmp;
	if(changedlist.size()!=0){
		tmp+='\n'+changedlist.join("\n")+'\n';
		changedlist.clear();
	}
	else tmp="Nothing changed";
	return tmp;
}
void Core::onmessage(){
	while (udpSocket->hasPendingDatagrams()) {
		QByteArray datagram;
		datagram.resize(udpSocket->pendingDatagramSize());
		QHostAddress host;
		udpSocket->readDatagram(datagram.data(), datagram.size(),&host);
		QStringList message=QString(datagram.data()).split(' ',QString::SkipEmptyParts);
		if(message[0]=="Hello"){
			unsigned int port=message[1].toUInt();
			QTcpSocket *socket=new QTcpSocket;
			socket->connectToHost(QHostAddress(host.toIPv4Address()), port);
			if(socket->waitForConnected(3000)){
				QString message1=sendmessage();
				const char * tosendmessage=message1.toStdString().c_str();
				socket->write(tosendmessage);
				socket->waitForBytesWritten(500);
				socket->close();
			}
		}
	}
}
void Core::filechanged(const QString &filename){
	QString str=QTime::currentTime().toString();
	str+=" "+filename;
	QFile * file=new QFile(filename);
	if (file->open(QIODevice::ReadOnly) ){
		str+=" was modified. New size: ";
		str+=QString::number(file->size() );
	}else str+=" was deleted";
	changedlist.append(str);
	file->close();
}
void Core::directorychanged(const QString & path){
// Compare the latest contents to saved contents for the dir updated to find out the difference(change)
	QStringList currEntryList = contents[path];
	const QDir dir(path);
	QStringList newEntryList = dir.entryList(QDir::NoDotAndDotDot  | QDir::AllDirs | QDir::Files, QDir::DirsFirst);
	QSet<QString> newDirSet = QSet<QString>::fromList( newEntryList );
	QSet<QString> currentDirSet = QSet<QString>::fromList( currEntryList );
	// Files that have been added
	QSet<QString> newFiles = newDirSet - currentDirSet;
	QStringList newFile = newFiles.toList();
	// Files that have been removed
	QSet<QString> deletedFiles = currentDirSet - newDirSet;
	QStringList deleteFile = deletedFiles.toList();
	// Update the current set
	contents[path] = newEntryList;
	if(!newFile.isEmpty() && !deleteFile.isEmpty() ){
		// File/Dir is renamed
		if(newFile.count() == 1 && deleteFile.count() == 1){
			QString str=QTime::currentTime().toString();str+=" "+deleteFile.first()+" was renamed to "+newFile.first()+".";
			changedlist.append(str);
		}
	}
	else{
		// New File/Dir Added to Dir
		if(!newFile.isEmpty() ){
			QString str=QTime::currentTime().toString();
			str+=" "+path+"/"+newFile.join(","+path+"/")+" was added.";
			changedlist.append(str);
			foreach(QString file, newFile){
				fsWatcher->addPath(path+"/"+file);
			}
		}
		// File/Dir is deleted from Dir
		if(!deleteFile.isEmpty() ){
			QString str=QTime::currentTime().toString();
			//str+=" "+path+"/"+deleteFile.join(","+path+"/")+" was deleted.";
			//changedlist.append(str);
			foreach(QString file, deleteFile){
				fsWatcher->removePath(path+"/"+file);
			}
		}
	}
}
void Core::ParseOptions(QFile * cfgfile){
	bool error = false;
	if(!(cfgfile->open(QIODevice::ReadOnly | QIODevice::Text) ) ) {
		qDebug() << "Error loading settings";
		error=true;
	}
	else{
		QTextStream in(cfgfile);
		while(!in.atEnd() ) {
			QStringList list=(in.readLine() ).split(' ',QString::SkipEmptyParts);
			QString id, eq, val;
			if (list.size()!=3 && list.size()!=0){
				error = true;
			}
			else if (list.size()==0||list[0][0] == '#'){
				continue;
			}
			else{
				id=list[0];eq=list[1];val=list[2];
				if ( eq != "=" ){
					error = true;
				}
			}
			if (error){
				options.clear();
				qDebug() << "Error was caught while loading settings";
			}
			else{
				options[id] = val;
			}
		}
		if(!error) return;
	}
	options["directory"]="/home/user";
	options["port"]="2048";
}
void Core::run(){
	exec();
}
