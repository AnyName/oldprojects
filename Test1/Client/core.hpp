#ifndef CORE_HPP
#define CORE_HPP

#include <QFileSystemWatcher>			//QFileSystemWatcher Class
#include <QMap>
#include <QFile>
#include <QThread>
#include <QUdpSocket>
class Core: public QThread
{
	Q_OBJECT

public:
	Core();
	void run();
private:
	void Send();
	QFileSystemWatcher *fsWatcher;
	QString sendmessage();
	void ParseOptions(QFile*);
	QMap<QString, QString> options;
	QMap<QString, QStringList> contents;
	QStringList changedlist;
	QUdpSocket * udpSocket;
private slots:
	void onmessage();
	void filechanged(const QString &);
	void directorychanged(const QString &);
};

#endif // CORE_HPP
