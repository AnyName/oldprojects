#include "server.hpp"
#include <QTcpSocket>
#include <QTime>
#include <iostream>
#include <QFile>
Server::Server(int port,QThread *parent) : QThread(parent),port(port)
{
	moveToThread(this);
}

void Server::broadcast()
{
	QByteArray datagram = "Hello "+ QByteArray::number(port);
	udpSocket->writeDatagram(datagram.data(), datagram.size(),QHostAddress::Broadcast, 4096);
}

void Server::run(){
	timer = new QTimer(0);
	udpSocket = new QUdpSocket(this);
	udpSocket->bind(QHostAddress::Any, 4096);
	connect(timer, SIGNAL(timeout() ), this, SLOT(broadcast() ) );
	timer->start(10000);
	server=new QTcpServer();
	connect(server, SIGNAL(newConnection()), this, SLOT(newConnection()));
	server->listen(QHostAddress::Any, port);
	exec();
}

void Server::newConnection(){				///TODO:Error handling
	QTcpSocket *socket = server->nextPendingConnection();
	socket->waitForReadyRead(500);
	char buffer[1024] = {0};
	socket->read(buffer, socket->bytesAvailable());
	QFile f("log.txt");
	if (f.open(QIODevice::WriteOnly | QIODevice::Append)) {
		f.write(std::string(QTime::currentTime().toString().toStdString()+std::string("Got message: ")+buffer+"\n").c_str());
	}
	std::cout << QTime::currentTime().toString().toStdString() << " Got message: \"" << buffer << "\"" <<std::endl;
	socket->flush();
	socket->close();
}
