#ifndef SERVER_HPP
#define SERVER_HPP

#include <QThread>
#include <QTcpServer>
#include <QTimer>
#include <QUdpSocket>
class Server : public QThread
{
	Q_OBJECT
public:
	explicit Server(int,QThread *parent = nullptr);

signals:

private slots:
	void run();
	void newConnection();
	void broadcast();

protected:
	QTimer *timer;
	QUdpSocket *udpSocket;
	QTcpServer *server;
	int port;
};

#endif // SERVER_HPP
