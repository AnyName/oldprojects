#!/bin/bash
filepath=/data/files
logpath=/var/log/sync.log
archpath=/data/to_sent/
files=$(find $filepath -mtime -7 | tail -n +2 | xargs ls -t | sed -n '1p;2p')
if [[ -z $files ]]; then
	exit 1
fi
for i in $files; do
        echo $(date)\; directory:$(dirname $i)\; name:$(basename $i)\; size:$( du -bh $i | awk '{print $1}') Bytes >> $logpath
	if [[ -z $(tar -czf $archpath$(basename $i).tar.gz -C $filepath $(basename $i)) ]]; then
		rm $i
	fi
done

if [[ -z $(crontab -l | grep $(readlink -f "$0")) ]]; then
	if [[ -z $(crontab -l 2>&1 >/dev/null | grep -v "no crontab for") ]];then
		(crontab -l; echo "20 04 * * * "$(readlink -f "$0") ) | crontab -
	fi
fi
