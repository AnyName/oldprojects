#ifndef AUTOINCREMENTHASHTABLE_HPP
#define AUTOINCREMENTHASHTABLE_HPP
#include <QFile>
//Хэш-таблица с автоувеличивающимся ключем.
template<typename T>
class AutoIncrementTable{
public:
	AutoIncrementTable(){
		increment=0;
	}

	void insert(T &item){
		table.insert(increment,item);
		increment++;
	}

	void clear(){
		increment=0;
		table.clear();
	}

	void save(QString filename){
		QFile file(filename);
		if ( file.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text) ){
			QTextStream stream(&file);
			for(unsigned long i=0;i<increment;i++)if(table.contains(i)){
				stream << QString::number(i)<<'|';table[i].save(stream);
			}
			file.close();
		}
	}
	void load(QString filename){
		QFile file(filename);
		if (file.open(QFile::ReadOnly | QFile::Text)){
			QTextStream in(&file);
			QString stream=in.readAll();
			QStringList splitted=stream.split("\n",QString::SkipEmptyParts);
			int key;
			for(int i=0;i<splitted.size();i++){
				QStringList split2=splitted[i].split("|",QString::SkipEmptyParts);
				key=split2[0].toInt();
				T item;
				item.load(split2[1]);
				table.insert(key,item);
			}
			increment=key+1;
			file.close();
		}
	}
	bool contains(int i){
		return table.contains(i);
	}

	T& operator[](int i){
		return table[i];
	}

	size_t size(){
		return table.size();
	}

	void remove(int i){
		table.remove(i);
	}

	unsigned long getIncrement(){
		return increment;
	}

protected:
	QHash<int,T> table;
	unsigned long increment;
};
#endif // AUTOINCREMENTHASHTABLE_HPP
