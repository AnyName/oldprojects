#include "connectionhashmap.hpp"
#include <QTextStream>
void ConnectionHashMap::addConnection(int prof_id,int stud_id){
	if(connection.contains(prof_id)){
		if(connection[prof_id].contains(stud_id))return;
	}
	else {QSet<int> set;connection.insert(prof_id,set);}
	connection[prof_id].insert(stud_id);
}
bool ConnectionHashMap::deleteConnection(int prof_id,int stud_id){
	   if(connection[prof_id].contains(stud_id)){
		   connection[prof_id].erase(connection[prof_id].find(stud_id));
		   return true;
	   }
	   return false;
}
bool ConnectionHashMap::deleteAllPConnections(int id){
	if(connection[id].empty())return false;
	connection[id].clear();
	return true;
}
bool ConnectionHashMap::deleteAllSConnections(int id){
	bool haveAtLeastOneOccurence=false;
	for(QHash<int,QSet<int>>::iterator it=connection.begin();it!=connection.end();it++)
	if(it.value().contains(id)){
		it.value().erase(it.value().find(id));
		if(!haveAtLeastOneOccurence)haveAtLeastOneOccurence=true;
	}
	return haveAtLeastOneOccurence;
}
QSet<int> * ConnectionHashMap::operator[](int prof_id){
	return &connection[prof_id];
}
void ConnectionHashMap::clear(){
	connection.clear();
}
void ConnectionHashMap::save(QString filename){
	QFile file(filename);
	if ( file.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text) ){
		QTextStream stream(&file);
		for(int i=0;i<*std::max_element(connection.keyBegin(),connection.keyEnd())+1;i++)if(connection.contains(i)&&connection[i].size()>0){
			stream << QString::number(i) << '|';
			for(int j=0;j<*std::max_element(connection[i].begin(),connection[i].end())+1;j++)if(connection[i].contains(j)){
				stream << '@' << QString::number(j);
			}
			stream << "\n";
		}
		file.close();
	}
}
void ConnectionHashMap::load(QString filename){
	QFile file(filename);
	if (file.open(QFile::ReadOnly | QFile::Text)){
		QTextStream in(&file);
		QString stream=in.readAll();
		QStringList splitted=stream.split("\n",QString::SkipEmptyParts);
		for(int i=0;i<splitted.size();i++){
			QStringList split2=splitted[i].split("|",QString::SkipEmptyParts);
			int key=split2[0].toInt();
			QSet<int> list;
			QStringList split3=split2[1].split("@",QString::SkipEmptyParts);
			for(int i=0;i<split3.size();i++)list.insert(split3[i].toInt());
			connection.insert(key,list);
		}
		file.close();
	}
}
