#ifndef CONNECTIONHASHMAP_HPP
#define CONNECTIONHASHMAP_HPP
#include <QSet>
#include <QFile>
//Подчинённая таблица связей типа "1 ко многим"
class ConnectionHashMap{
public:
	QHash<int,QSet<int>> connection;
	void addConnection(int,int);
	bool deleteConnection(int,int);
	bool deleteAllPConnections(int);
	bool deleteAllSConnections(int);
	QSet<int> * operator[](int);
	void clear();
	void save(QString);
	void load(QString);
};

#endif // CONNECTIONHASHMAP_HPP
