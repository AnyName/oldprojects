#include "degreespinbox.hpp"
DegreeSpinBox::DegreeSpinBox(QWidget *parent)
	: QSpinBox(parent)
{
}
int DegreeSpinBox::valueFromText(const QString &text) const{
	for(int i=0;i<3;i++)if(text==convertDegree(i)){
		return i;
	}
	return -1;
}
QString DegreeSpinBox::textFromValue(int value) const{
	return convertDegree(value);
}
