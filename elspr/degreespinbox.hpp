#ifndef DEGREESPINBOX_HPP
#define DEGREESPINBOX_HPP
#include <QSpinBox>
QString convertDegree(int); //Имплементация в mainwindow.cpp


class DegreeSpinBox : public QSpinBox
{
	Q_OBJECT

public:
	DegreeSpinBox(QWidget *parent = 0);

	int valueFromText(const QString &text) const;
	QString textFromValue(int value) const;
};

#endif // DEGREESPINBOX_HPP
