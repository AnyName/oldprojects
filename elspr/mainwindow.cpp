#include "mainwindow.hpp"
#include "ui_mainwindow.h"
#include <QtConcurrent/QtConcurrent>
#include <QStandardItemModel>
MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);
//Зарплата только числовым.
	ui->editPSalary->setValidator( new QIntValidator(0, 100000, this) );
//Загрузка данных из файла при запуске.
	on_loadButton_clicked();
}

MainWindow::~MainWindow()
{
	delete ui;
}

//Перевод из инт в учёную степень
QString convertDegree(int degree){
	switch (degree) {
		case 0:
			return "Отсутствует";
		break;
		case 1:
			return "Кандидат наук";
		break;
		case 2:
			return "Доктор наук";
		break;
		default:
			return "Error";
		break;
	}
}

//Пересоздание таблиц.
void MainWindow::updateStudentsTable(){
	QStandardItemModel *model = new QStandardItemModel(0,2,this);
	model->setHorizontalHeaderItem(0, new QStandardItem(QString("id")));
	model->setHorizontalHeaderItem(1, new QStandardItem(QString("ФИО")));
	ui->tableStudents->setSelectionMode(QAbstractItemView::SingleSelection);
	ui->tableStudents->setEditTriggers(QAbstractItemView::NoEditTriggers);
	ui->tableStudents->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	for(unsigned long i=0;i<students.getIncrement();i++)if(students.contains(i)){
		QList<QStandardItem*> row;
		row.append(new QStandardItem(QString::number(i)));
		row.append(new QStandardItem(students[i].surname+QString(' ')+students[i].name+QString(' ')+students[i].patronymic));
		model->appendRow(row);
	}
	ui->tableStudents->setModel(model);
	ui->tableStudents->setColumnHidden(0,1);
	updateProfessorStudentsTable();
}

void MainWindow::updateProfessorsTable(){
	QStandardItemModel *model = new QStandardItemModel(0,2,this);
	model->setHorizontalHeaderItem(0, new QStandardItem(QString("id")));
	model->setHorizontalHeaderItem(1, new QStandardItem(QString("ФИО")));
	ui->tableProfessors->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	ui->tableProfessors->setSelectionMode(QAbstractItemView::SingleSelection);
	ui->tableProfessors->setEditTriggers(QAbstractItemView::NoEditTriggers);
	for(unsigned long i=0;i<professors.getIncrement();i++)if(professors.contains(i)){
		QList<QStandardItem*> row;
		row.append(new QStandardItem(QString::number(i)));
		row.append(new QStandardItem(professors[i].surname+QString(' ')+professors[i].name+QString(' ')+professors[i].patronymic));
		model->appendRow(row);
	}
	ui->tableProfessors->setModel(model);
	ui->tableProfessors->setColumnHidden(0,1);
	updateProfessorStudentsTable();
}

void MainWindow::updateProfessorStudentsTable(){
	ui->tableProfessorStudents->setColumnCount(2);
	ui->tableProfessorStudents->setRowCount(students.size());
	ui->tableProfessorStudents->setHorizontalHeaderItem(0, new QTableWidgetItem(QString("id")));
	ui->tableProfessorStudents->setHorizontalHeaderItem(1, new QTableWidgetItem(QString("ФИО")));
	ui->tableProfessorStudents->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	ui->tableProfessorStudents->setEditTriggers(QAbstractItemView::NoEditTriggers);
	ui->tableProfessorStudents->setSelectionMode(QAbstractItemView::SingleSelection);
	for(unsigned long i=0,it=0;i<students.getIncrement();i++)
	if(students.contains(i)){
		ui->tableProfessorStudents->setItem(it,0,new QTableWidgetItem(QString::number(i)));
		ui->tableProfessorStudents->setItem(it,1,new QTableWidgetItem(students[i].surname+QString(' ')+students[i].name+QString(' ')+students[i].patronymic));
		if(connection[professorindex]->contains(i)) ui->tableProfessorStudents->item(it,1)->setBackground(Qt::green);
		else ui->tableProfessorStudents->item(it,1)->setBackground(Qt::gray);
		it++;
	}
	ui->tableProfessorStudents->setSelectionMode(QTableWidget::NoSelection);
	ui->tableProfessorStudents->setColumnHidden(0,1);
}



void MainWindow::on_loadButton_clicked()
{
	clearProf();
	clearStud();
	students.clear();
	professors.clear();
	connection.clear();
	professors.load("professors.txt");
	students.load("students.txt");
	connection.load("connection.txt");
	updateStudentsTable();
	updateProfessorsTable();
}
void MainWindow::clearProf(){
	professorindex=-1;
	ui->editPName->setText("");
	ui->editPSurname->setText("");
	ui->editPPatronymic->setText("");
	ui->editPSalary->setText("");
	ui->spinBoxPAD->setValue(0);
	ui->PSave->setEnabled(0);
	ui->PDelete->setEnabled(0);
}
void MainWindow::clearStud(){
	studentindex=-1;
	ui->editSName->setText("");
	ui->editSSurname->setText("");
	ui->editSPatronymic->setText("");
	ui->SBirthday->setDate(QDate(1970,1,1));
	ui->checkScholarship->setChecked(false);
	ui->SSave->setEnabled(0);
	ui->SDelete->setEnabled(0);
}

void MainWindow::on_saveButton_clicked()
{
	professors.save("professors.txt");
	students.save("students.txt");
	connection.save("connection.txt");
}

void MainWindow::on_tableProfessors_clicked(const QModelIndex &index)
{
	if(professorindex!=index.row()){
		//Отключение кнопок удаления и сохранения изменений
		professorindex=ui->tableProfessors->model()->index(index.row(),0).data().toInt();
		if(professorindex==-1){ui->PSave->setEnabled(0);ui->PDelete->setEnabled(0);}
		else {ui->PSave->setEnabled(1);ui->PDelete->setEnabled(1);}
		//Чтение данных из professors
		updateProfessorStudentsTable();
		ui->editPName->setText(professors[professorindex].name);
		ui->editPSurname->setText(professors[professorindex].surname);
		ui->editPPatronymic->setText(professors[professorindex].patronymic);
		ui->editPSalary->setText(QString::number(professors[professorindex].salary));
		ui->spinBoxPAD->setValue(professors[professorindex].academic_degree);
	}
}

void MainWindow::on_tableStudents_clicked(const QModelIndex &index)
{
	if(studentindex!=index.row()){
		//Отключение кнопок удаления и сохранения изменений
		studentindex=ui->tableStudents->model()->index(index.row(),0).data().toInt();
		if(studentindex==-1){ui->SSave->setEnabled(0);ui->SDelete->setEnabled(0);}
		else {ui->SSave->setEnabled(1);ui->SDelete->setEnabled(1);}
		//Чтение данных из students
		ui->editSName->setText(students[studentindex].name);
		ui->editSSurname->setText(students[studentindex].surname);
		ui->editSPatronymic->setText(students[studentindex].patronymic);
		ui->SBirthday->setDate(students[studentindex].birthday);
		ui->checkScholarship->setChecked(students[studentindex].scholarship);
	}
}

void MainWindow::on_SAdd_clicked()
{
	Student newstudent;
	newstudent.name=ui->editSName->text();newstudent.surname=ui->editSSurname->text();newstudent.patronymic=ui->editSPatronymic->text();
	newstudent.scholarship=ui->checkScholarship->isChecked();newstudent.birthday=ui->SBirthday->date();
	students.insert(newstudent);
	updateStudentsTable();
}

void MainWindow::on_SSave_clicked()
{
	if(students.contains(studentindex)){
		students[studentindex].name=ui->editSName->text();students[studentindex].surname=ui->editSSurname->text();students[studentindex].patronymic=ui->editSPatronymic->text();
		students[studentindex].scholarship=ui->checkScholarship->isChecked();students[studentindex].birthday=ui->SBirthday->date();
		updateStudentsTable();
	}
}

void MainWindow::on_SDelete_clicked()
{
	if(students.contains(studentindex)){
		if(connection.deleteAllSConnections(studentindex))updateProfessorStudentsTable();
		students.remove(studentindex);
		clearStud();
		updateStudentsTable();
	}
}
void MainWindow::on_PAdd_clicked()
{
	Professor newprofessor;
	newprofessor.name=ui->editPName->text();
	newprofessor.surname=ui->editPSurname->text();
	newprofessor.patronymic=ui->editPPatronymic->text();
	newprofessor.salary=ui->editPSalary->text().toInt();
	newprofessor.academic_degree=ui->spinBoxPAD->value();
	professors.insert(newprofessor);
	for(int i=0;i<ui->tableProfessorStudents->model()->rowCount();i++)
	if(ui->tableProfessorStudents->item(i,1)->background()==Qt::green){
		connection.addConnection(professorindex,ui->tableProfessorStudents->model()->index(i,0).data().toInt());
	}
	updateProfessorsTable();
}

void MainWindow::on_PSave_clicked()
{
	if(professors.contains(professorindex)){
		professors[professorindex].name=ui->editPName->text();
		professors[professorindex].surname=ui->editPSurname->text();
		professors[professorindex].patronymic=ui->editPPatronymic->text();
		professors[professorindex].salary=ui->editPSalary->text().toInt();
		professors[professorindex].academic_degree=ui->spinBoxPAD->value();
		connection.deleteAllPConnections(professorindex);
		for(int i=0;i<ui->tableProfessorStudents->model()->rowCount();i++)
		if(ui->tableProfessorStudents->item(i,1)->background()==Qt::green){
			connection.addConnection(professorindex,ui->tableProfessorStudents->model()->index(i,0).data().toInt());
		}
		updateProfessorsTable();
	}
}

void MainWindow::on_PDelete_clicked()
{
	if(professors.contains(professorindex)){
		connection.deleteAllPConnections(professorindex);
		professors.remove(professorindex);
		clearProf();
		updateProfessorsTable();
	}
}

void MainWindow::on_tableProfessorStudents_cellClicked(int row, int column)
{
	if(ui->tableProfessorStudents->item(row,column)->background()==Qt::gray){
		ui->tableProfessorStudents->item(row,column)->setBackground(Qt::green);
	}
	else {
		ui->tableProfessorStudents->item(row,column)->setBackground(Qt::gray);
	}
}
