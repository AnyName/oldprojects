#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP
#include <QMainWindow>
#include "professor.hpp"
#include "student.hpp"
#include "autoincrementhashtable.hpp"
#include "connectionhashmap.hpp"
namespace Ui {
	class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();
	AutoIncrementTable<Student> students;
	AutoIncrementTable<Professor> professors;
	ConnectionHashMap connection;
	int professorindex,studentindex;
	void clearProf(),clearStud();
	void updateStudentsTable();
	void updateProfessorsTable();
private slots:

	void on_loadButton_clicked();

	void on_saveButton_clicked();

	void on_tableProfessors_clicked(const QModelIndex &);

	void on_tableStudents_clicked(const QModelIndex &);

	void on_SAdd_clicked();

	void on_SSave_clicked();

	void on_SDelete_clicked();

	void on_PAdd_clicked();

	void on_PSave_clicked();

	void on_PDelete_clicked();

	void on_tableProfessorStudents_cellClicked(int, int);

private:
	int id;
	void updateProfessorStudentsTable();
	Ui::MainWindow *ui;
};


#endif // MAINWINDOW_HPP
