#include "professor.hpp"
Professor::Professor(){
	patronymic=surname=name="";
	academic_degree=salary=0;
}

void Professor::load(QString stream){
	QStringList splitted=stream.split("@",QString::SkipEmptyParts);
	name=splitted[0];
	surname=splitted[1];
	patronymic=splitted[2];
	academic_degree=splitted[3].toInt();
	salary=splitted[4].toInt();
}
void Professor::save(QTextStream &stream){
	stream << name << '@' << surname << '@' << patronymic << '@' << QString::number(academic_degree) << '@' << QString::number(salary) << '\n';
}
