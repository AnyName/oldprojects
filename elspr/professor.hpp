#ifndef PROFESSOR_HPP
#define PROFESSOR_HPP
#include <QStringList>
#include <QTextStream>

class Professor{
public:
	Professor();
	QString name,surname,patronymic;
	int academic_degree;
	unsigned long salary;
	void save(QTextStream&);
	void load(QString);
};

#endif // PROFESSOR_HPP
