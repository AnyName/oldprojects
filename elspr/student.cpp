#include "student.hpp"
Student::Student(){
	patronymic=surname=name="";
	scholarship=0;
	birthday=QDate(1970,1,1);
}
void Student::save(QTextStream &stream){
	stream << name << '@' << surname << '@' << patronymic << '@' << QString::number(scholarship) << '@' << birthday.toString("ddMMyyyy") << '\n';
}
void Student::load(QString stream){
	QStringList splitted=stream.split("@",QString::SkipEmptyParts);
	name=splitted[0];
	surname=splitted[1];
	patronymic=splitted[2];
	scholarship=splitted[3].toInt();
	birthday=QDate::fromString(splitted[4],"ddMMyyyy");
}
