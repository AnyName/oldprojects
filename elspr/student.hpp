#ifndef STUDENT_HPP
#define STUDENT_HPP
#include <QTextStream>
#include <QDate>

class Student{
public:
	Student();
	QString name,surname,patronymic;
	bool scholarship;
	QDate birthday;
	void save(QTextStream&);
	void load(QString);
};

#endif // STUDENT_HPP
