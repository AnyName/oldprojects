Usage: threads BLOCKTHREADNUM BLOCKNUM BLOCKSIZE CRCTHREADNUM
This program will create BLOCKTHREADNUM amount of threas, which will create BLOCKNUM blocks of random data with size of BLOCKSIZE.
CRCTHREADNUM amount of threads will calculate CRC for those blocks.
After all CRC threads finishes calculating CRC for any block - compare CRCs.
And if they are not equal - write that block to file and send a message to cout.
