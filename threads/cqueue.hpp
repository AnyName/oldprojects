#ifndef CQUEUE_HPP
#define CQUEUE_HPP
//STL queue (deque) with iterators cannot guarantee thread safety (Resizing will break iterators). C queue with posize_ters instead will work better in that exact situation
//Also, more thread safety with mutexes

#include <mutex>

template<typename T>
class CQueue
{
public:
    class Datatype
    {
	public:
		Datatype(T data):data(data){
			refcount=0;
			next=nullptr;
        }
		~Datatype(){
			next=nullptr;
			delete data;
        }
		T data;
		Datatype * next;
		size_t refcount;
	};
	std::mutex mutex;
	CQueue(){begin=nullptr;end=nullptr;size=0;}
	~CQueue(){}
    void push(T item){
		mutex.lock();
		Datatype * data=new Datatype(item);
		if(size++==0) {
			begin=data;
			end=data;
        }
		else {
			end->next=data;
			end=data;
		}
        mutex.unlock();
    }
	//While this function will get you the posize_ter to first element
    Datatype * pop(){
		if (size > 0) {
			mutex.lock();
			Datatype * prevbegin = begin;
			begin = prevbegin->next;
			size--;
			mutex.unlock();
			return prevbegin;
		}
		return nullptr;
    }
	//This function will just delete it and free memory
	void popdelete(){
		delete pop();
    }
	void clear(){
        if(size>0)
			for(size_t i=0;i<size;i++) popdelete();
	}
	Datatype *begin,*end;
	size_t size;
};

#endif // CQUEUE_HPP
