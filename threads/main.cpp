#include "program.hpp"
int main(int argc, const char *argv[])
{
	srand(time(NULL));
	if (argc != 5) {
		std::cout <<	"\nUsage: threads BLOCKTHREADNUM BLOCKNUM BLOCKSIZE CRCTHREADNUM\n"
				"\nThis program will create BLOCKTHREADNUM amount of threas, which will create BLOCKNUM blocks of random data with size of BLOCKSIZE."
				"\nCRCTHREADNUM amount of threads will calculate CRC for those blocks."
				"\nAfter all CRC threads finishes calculating CRC for any block - compare CRC."
				"\nAnd if they are not equal - write that block to file and send a message to cout\n";
		return 1;
	}
	Program* program = new Program(argc,argv);
	program->run();
	delete program;
#ifdef VERBOSE
	std::cout << "Program finished" << std::endl;
#endif
	return 0;
}
