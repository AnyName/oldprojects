#include "program.hpp"
#include <sstream>				//sstream
#include "crc32.hpp"

#define GETDATA (*iter->data)

Program::Program(int argc, const char *argv[])
{
	blockthreadnum=atoi(argv[1]), blocknum=atoi(argv[2]), blocksize=atoi(argv[3]), crcthreadnum=atoi(argv[4]);
	workingblockthreads=blockthreadnum;
	workingcalcthreads=crcthreadnum;
	file.open ("missmatch.txt");
}
Program::~Program(){
	blockthreads.clear();
	calculatorthreads.clear();
	dqueue.clear();
	file.close();
}
size_t calculate_CRC32(const std::deque<unsigned char> &buffer)
{
  return crc32<IEEE8023_CRC32_POLYNOMIAL>(0xFFFFFFFF, buffer.begin(), buffer.end());
}

void Program::datablockcreaterthread(size_t currentthreadnum) {
	for (size_t i=0;i<blocknum;i++)
	{
		deque<unsigned char> * data = new deque<unsigned char> (blocksize);
		for(size_t j=0;j<blocksize;j++)
			data->at(j)=rand()%256;
		Data * qdata = new Data(data,crcthreadnum);
		dqueue.push(qdata);
	}
	programmutex.lock();
	workingblockthreads--;
	programmutex.unlock();
#ifdef VERBOSE
	cout << "Datablock thread # " << currentthreadnum << " Finished" << endl;
#endif

}

void Program::crccalculatorthread(size_t currentthreadnum) {
	while(dqueue.begin==nullptr)
		this_thread::sleep_for(std::chrono::milliseconds(20));
	watchmutex.lock();
	CQueue<Data*>::Datatype * iter=dqueue.begin;
	iter->refcount++;
	watchmutex.unlock();
#ifdef VERBOSE
	size_t iteration=1;
#endif
	while ( (iter!= nullptr) ) {
		while ( (
					( workingblockthreads != 0 ) || ( dqueue.size != 0 )
				) && (
					! (
						(iter->next != nullptr)
						||	(
							( workingblockthreads == 0 ) && (dqueue.size==1) && (iter==dqueue.end))
						)
					)
				)
			this_thread::sleep_for(std::chrono::milliseconds(20));
#ifdef VERBOSE
		cout << "Thread # " << currentthreadnum << " iteration " << iteration << endl;
#endif
		uint32_t crc=calculate_CRC32(*(GETDATA.data));
#ifdef VERBOSE
		cout << "Thread # " << currentthreadnum << endl;
		cout << "CRC32 : " << crc << endl;
		iteration++;
#endif
		GETDATA.crc32->at(currentthreadnum)=crc;
		watchmutex.lock();
		GETDATA.calculated++;
		iter->refcount--;
		iter=iter->next;
		if (iter!=nullptr)
			iter->refcount++;
		watchmutex.unlock();
	}
	programmutex.lock();
	workingcalcthreads--;
	programmutex.unlock();
#ifdef VERBOSE
	cout << "CRC Calculator thread # " << currentthreadnum << " Finished" << endl;
#endif
}



void Program::run(){
	for(size_t i=0;i<blockthreadnum;i++) {
		blockthreads.push_back(new thread(&Program::datablockcreaterthread,this,i));
	};
	for(size_t i=0;i<crcthreadnum;i++) {
		calculatorthreads.push_back(new thread(&Program::crccalculatorthread,this,i));
	};
	while(dqueue.begin==nullptr)
		this_thread::sleep_for(std::chrono::milliseconds(20));
	CQueue<Data*>::Datatype * iter=dqueue.begin;
	while (
		   (workingblockthreads > 0) || ( dqueue.size != 0 )
		  ) {
		if (
			(
				(iter->next!=nullptr) || (
					(dqueue.size==1) && (dqueue.end==iter)
				)
			)	&&
				(GETDATA.calculated == crcthreadnum) && (
					(dqueue.begin!=dqueue.end) || (dqueue.size==1)
				) && (iter->refcount==0)
			)
		{
			uint32_t ccrc=*(GETDATA.crc32->begin());
			for (size_t i=1;i<crcthreadnum;i++) {
				if (ccrc!= GETDATA.crc32->at(i)) {
					cout << "CRC32 Missmatch: " << ccrc << " and " << GETDATA.crc32->at(i) << endl;
					std::stringstream output;
					for (i=0;i<crcthreadnum;i++)
						output << (size_t)GETDATA.data->at(i) << " ";
					output << endl;
					file << output.str();
					break;
				}
			}
			iter=iter->next;
			watchmutex.lock();
			dqueue.popdelete();
			watchmutex.unlock();
		}
	}
}
