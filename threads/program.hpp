#ifndef PROGRAM_HPP
#define PROGRAM_HPP

#include <thread>
#include <atomic>
#include <iostream>
#include <fstream>
#include <deque>
#include <vector>
//I didnt know before that deque/vector iterators gets invalidated when contaner resizes itself so i tried to use deque before
//That caused segfaults when deque/vector iterator got invalidated (container got resized)
//Afterwards i realised that C-like FIFO queue will work better in that case, because pointers *next and *prev wont get invalidated
#define VERBOSE
#include "cqueue.hpp"

using namespace std;
class Program
{
public:
		Program(int, const char **);
		~Program();
        void run();
private:
		size_t blocksize,blocknum,blockthreadnum,crcthreadnum;
		mutex writemutex,watchmutex,programmutex;
		size_t workingblockthreads,workingcalcthreads;
		void datablockcreaterthread(size_t);
		void crccalculatorthread(size_t);
		class Data {  //Can be reused as a template. Just change 'unsigned char' to typename def
		public:
			Data(deque<unsigned char>* data,size_t num):data(data){
				crc32=new vector<uint32_t>(num,0);
			}
			~Data () {
				data->clear();
				delete data;
				crc32->clear();
				delete crc32;
#ifdef VERBOSE
				cout << "DATA FREED" << endl;
#endif
			}
			deque<unsigned char> * data;
			vector<uint32_t> * crc32;
			size_t calculated=0;
        };
        deque<thread*> blockthreads;
		deque<thread*> calculatorthreads;
		CQueue<Data*> dqueue;
		ofstream file;
};

#endif // PROGRAM_HPP
